#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import logging
import re
from pathlib import Path
from shutil import copyfile

import pandas as pd
import pkg_resources
from tqdm import tqdm

import epitran

tqdm.pandas()
logging.disable(logging.NOTSET)


def add_rules(dir, code, reverse=False):
    """ Move rules files to Epitran's data directory

    Args:
        dir (str): path to the directory where the rules are stored.
        code (str): ISO 639-3 plus "-" plus ISO 15924 code of the
                    language/script pair that should be loaded
        reverse (bool): whether to also move reverse files
    """
    paths = [f"map/{code}.csv",
             f"pre/{code}.txt",
             f"post/{code}.txt"]

    if reverse:
        paths.extend([f"map/{code}_rev.csv", f"pre/{code}_rev.txt",
                      f"post/{code}_rev.txt"])
    for path in paths:
        pkg_files = pkg_resources.resource_filename("epitran", f"data/{path}")
        copyfile(f"{dir}/{path}", pkg_files)


def get_epitran_instance():
    rule_folder = str(Path("./epitran").resolve())
    add_rules(rule_folder, "kat-Geor", reverse=False)
    epi = epitran.Epitran('kat-Geor')
    return epi

def debug(word):
    logging.getLogger("epitran").setLevel(logging.DEBUG)
    epi = get_epitran_instance()
    logging.info(f"Transcribing {word}:")
    return epi.transliterate(word)

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--transcribe",
                        help="Transcribe (otherwise, stop after previous steps)",
                        action="store_true")


    parser.add_argument("--de_duplicate",
                        help="Remove forms duplicates (not incl index)",
                        action="store_true")

    parser.add_argument("--gen_cells",
                        help="Re-generate unique cells table from forms",
                        action="store_true")

    parser.add_argument("--debug", type=str,
                        help="Debug transcription by showing the application of each rule", default=None)

    args = parser.parse_args()

    if args.debug is not None:
        debug(args.debug)

    if args.transcribe:
        df = pd.read_csv("forms.csv")
        df = transcribe_lexicon(df)
        df.to_csv("forms.csv", index=False)

    if args.de_duplicate:
        df = pd.read_csv("forms.csv", index_col=0)
        dups = df[df.duplicated(keep=False)]
        dups.to_csv("duplicates.csv")
        df.drop_duplicates().to_csv("forms.csv", index=True)
    if args.gen_cells:
        df = pd.read_csv("forms.csv")
        cells = df.loc[:, ["cell"]].drop_duplicates().sort_values("cell")
        cells.rename({"cell":"cell_id"}, axis=1, inplace=True)
        cells.to_csv("cells.csv", index=False)

    print("Done !")



def transcribe_lexicon(df):
    sounds = pd.read_csv("./sounds.csv")
    sounds = sorted(sounds.sound_id.tolist(), key=len, reverse=True)
    sound_reg = "(" + "|".join(sounds) + ")"

    def split(f):
        f2 = list(re.split(sound_reg, f))
        return " ".join([x for x in f2 if x])

    print("Transcription")
    epi = get_epitran_instance()
    logging.disable(logging.DEBUG)
    df.loc[:, "phon_form"] = df.orth_form.progress_apply(epi.transliterate).apply(split)

    return df


main()
