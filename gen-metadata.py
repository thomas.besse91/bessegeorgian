from paralex import paralex_factory

package = paralex_factory("Georgian Verbal Paradigms in Phonemic Notation",
                          {"cells": {"path": "cells.csv"},
                           "forms": {"path": "forms.csv"},
                           "lexemes": {"path": "lexemes.csv"},
                           "sounds": {"path": "sounds.csv"},
                           "features-values": {"path": "feature-values.csv"},
                           },
                          contributors=[
                              {'title': 'Thomas Besse', 'organization': 'U. Paris Cité'},
                          ],
                          licenses=[{'name': 'CC-BY-NC-SA-4.0',
                                     'title': 'Attribution-NonCommercial-ShareAlike 4.0 International  (CC BY-NC-SA 4.0) ',
                                     'path': 'https://creativecommons.org/licenses/by-nc-sa/4.0/'}],
                          keywords=["georgian", "verbs", "paradigms", "paralex"],
                          name="georgian-verbal-paradigms",
                          version="1.0.0"
                          )

package.infer()
package.to_json("georgian.package.json")
